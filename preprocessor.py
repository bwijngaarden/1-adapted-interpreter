#environments and jump map concept

def prep(program, fmap):
    pc = 0
    while pc < len(program):
        char = program[pc]
        if char == '(':
            parse_func(program[pc+1:], fmap)            
        pc += 1
    fmap["main"] = program

def parse_func(program_fragment, fmap):
    pc = 0
    end_name = 0
    while pc < len(program_fragment) and program_fragment[pc] != '|':
        #ignore function calls!
        if program_fragment[pc] == ')':
            return 0
        pc += 1
        end_name += 1
    name = program_fragment[0:end_name]
    call = False
    while pc < len(program_fragment):
        if program_fragment[pc] == ')':
            if call:
                call = False
            else:
                break
        pc += 1
        if program_fragment[pc] == '(':
            offset = parse_func(program_fragment[pc+1:], fmap)
            if offset == 0:
                call = True
            pc = pc + offset
    body = program_fragment[end_name+1:pc]
    fmap[name] = body
    return pc + 2

def map_jumps(fmap, jmap):
    for key, body in fmap.items():
        pc = 0
        in_func = 0
        jump_index = 0
        while pc < len(body):
            if body[pc] == '(':
                in_func += 1
            elif body[pc] == ')': 
                in_func -= 1
            elif in_func == 0 and body[pc] == '#':
                jmap[(key, jump_index)] = body[pc+1:]
                jump_index += 1
            pc += 1
                
                

function_map = {}
prep(input("Program: "), function_map)
jump_map = {}
map_jumps(function_map, jump_map)
print(function_map)
print(jump_map)
